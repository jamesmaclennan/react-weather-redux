export default function(state=null, action) {
    // sample req: https://api.darksky.net/forecast/2486ab66ccdd11f08cf943897609fb90/49.2827,-123.1207?units=ca&exclude=[minutely,alerts,flags]
    let locationList = [];

    function locationManager() {
        if (!state) {
            return locationList;
        }
        let newLocationListing = state.slice();

        switch(action.type) {
            case 'ADD_LOCATION':
                // push new location to collection and return collection
                newLocationListing.push(action.payload);
                return newLocationListing
            case 'REMOVE_LOCATION':
                //remove location with matching index and return collection
                newLocationListing.splice(action.payload, 1)
                return newLocationListing
            default:
                return locationList;
        }
    }

    return locationManager();
}
