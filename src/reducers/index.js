import { combineReducers } from 'redux';
import LocationList from './reducer-locations';

const reducers = combineReducers({
    locations: LocationList
});

export default reducers;
