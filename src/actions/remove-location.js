export const removeLocation = (location) => {
    return {
        type: 'REMOVE_LOCATION',
        payload: location
    }
}
