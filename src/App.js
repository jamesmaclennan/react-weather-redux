import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { addLocation } from './actions/add-location.js';

import './App.css';

import Header from './components/header';
import LocationSelect from './containers/select'
import Locations from './containers/locations';

class App extends Component {
	componentDidMount() {
		document.title = 'React Weather Redux - James Maclennan';
		var self = this;

		let getCurrentLocationWeather = () => {

			function getPositionError(err) {
				console.warn(`ERROR(${err.code}): ${err.message}`);
			}

			let positionOptions = [
				'maximumAge': 0
			]

			navigator.geolocation.getCurrentPosition(function(position) {

				let darkSky = {
		            url: `https://weather-api.jamesmaclennan.com/location/${position.coords.latitude}/${position.coords.longitude}`
		        }
				fetch(darkSky.url)
		            .then((response) => response.json())
		            .then(function(responseJSON) {
		                self.props.addLocation(responseJSON);
		            })
			}, getPositionError)
		}

		if (navigator.geolocation) {
			getCurrentLocationWeather()
		}

	}
	render() {
	    return (
	    	<div className="App">
	        	<Header />

	        	<div className="select-wrap">
	            	<LocationSelect />
	            </div>
	            <Locations />
	        </div>
	    );
	}
}


function matchDispatchToProps(dispatch) {
    return bindActionCreators({addLocation: addLocation}, dispatch)
}

export default connect(null,matchDispatchToProps)(App);
