import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CSSTransitionGroup } from 'react-transition-group';

import Location from '../components/location';


class Locations extends Component {
    _buildLocationList() {
        return this.props.locations.map((location, index) => {
            return (
                <Location key={index} id={index} location={location} />
            )
        })
    }
    render() {
        return (
            <CSSTransitionGroup
                component="ul"
                className="locations"
                transitionName="locations"
                transitionEnterTimeout={500}
                transitionLeaveTimeout={300}>
                    {this._buildLocationList()}
            </CSSTransitionGroup>
        )
    }
}

function mapStateToProps(state) {
    return {
        locations: state.locations
    }
}

export default connect(mapStateToProps)(Locations);
