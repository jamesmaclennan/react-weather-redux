import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { addLocation } from '../actions/add-location.js';


import Select from 'react-select';

class locationSelect extends Component {

    _addLocation = (val) => {
        if (val) {
            var location = {
                name: val.name,
                label: val.label,
                coords: {
                    latitude: val.value.split(',')[0],
                    longitude: val.value.split(',')[1]
                }
            }

            // setup dark sky request parameters
            let darkSky = {
                url: `https://weather-api.jamesmaclennan.com/location/${location.coords.latitude}/${location.coords.longitude}`
            }


            // fetch forecast data
            fetch(darkSky.url)
                .then((response) => response.json())
                .then((responseJSON) => {
                    this.props.addLocation(responseJSON);
                })
        }
    }
    _typeAhead(input, callback) {
        // add typeahead options to add city select box
        var results = [];

        // only check if there's something in the text box
        if (input.length >= 1) {

            // setup search api request parameters
            let searchRequest = {
                url: 'https://weather-api.jamesmaclennan.com/search/' + input
            }

            fetch(searchRequest.url)
                .then((response) => response.json())
                .then((data) => {
                    data.map((result, index) => {
                        var location = result.city + ', ' + result.country;
                        var coords = result.latitude + ',' + result.longitude;
                        results.push({value: coords, label: location, name: result.city});
                        return(true);
                    });
                })
        }

        // limit rate of search requests
        setTimeout(function() {
            callback(null, {
                options: results,
                complete: false
            });
        }, 500);
    }
    render() {
        return (
            <Select.Async name="form-field-name" value='' onChange={this._addLocation} searchPromptText="Type City Name" placeholder="Add location..." loadOptions={this._typeAhead} autoBlur={true}/>
        )
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({addLocation: addLocation}, dispatch)
}

export default connect(null,matchDispatchToProps)(locationSelect);
