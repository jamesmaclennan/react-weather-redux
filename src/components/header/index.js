import React, { Component } from 'react';

class Header extends Component {
    // constructor(props) {
    //     super(props);
    // }

    render() {
        return (
            <header className="header-global">
                <h1 className="header-global__heading">react-weather-redux</h1>
            </header>
        )
    }
}

export default Header;
