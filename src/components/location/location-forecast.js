// this is the 7 day forecast component

import React, { Component } from 'react';
import moment from 'moment';
import ReactAnimatedWeather from 'react-animated-weather';
import { CSSTransitionGroup } from 'react-transition-group';
import Stagger from 'react-css-stagger';


class Forecast extends Component {
    _buildForecast() {

        // console.log(this.refs);

        // var city = this.refs.city.getDOMNode();
        // var forecast = this.refs.days.getDOMNode();

        // timeline.to(city.getElementsByClassName('wrap-icon'), 2.5, {transform: 'translateY(0)', opacity: 1, ease:Expo.easeOut}, '-=.25')
        // timeline.to(city.getElementsByClassName('wrap-info'), 3, {opacity: 1, ease:Quint.easeOut}, "-=2.5")
        // TimelineLite.staggerTo(forecast.children, .15, {transform: 'scale(1)'}, .05, '-=2.75');


        let forecastRange;
        this.props.hourly ? forecastRange = 'hourly' : forecastRange = 'daily';

        return this.props.location.weather[forecastRange].data.slice(1,8).map((item, i) => {

            switch(this.props.hourly) {
                // return the hourly forcast
                case true:
                    return (
                        <div key={i} className='forecast__item forecast__item-hourly'>
                            <span className="forecast__title">{moment.unix(item.time).tz(this.props.location.weather.timezone).format('hA')}</span>
                            <div className={'forecast__icon ' +  item.icon.replace(/-/g, '_').toUpperCase()}>
                                <ReactAnimatedWeather
                                    icon={item.icon.replace(/-/g, '_').toUpperCase()}
                                    size={128}
                                    color='#EF8354'

                                />
                            </div>
                            <span className="forecast__high">{Math.round(item.temperature)}</span>
                            <span className="forecast__low">&nbsp;</span>
                        </div>
                    )
                // return the daily forecast
                default:
                    return (
                        <div key={i} className='forecast__item'>
                            <span className="forecast__title">{moment.unix(item.time).format('ddd')}</span>
                            <div className="forecast__icon">
                                <ReactAnimatedWeather
                                    icon={item.icon.replace(/-/g, '_').toUpperCase()}
                                    size={128}
                                    color='#EF8354'
                                />
                            </div>
                            <span className="forecast__high">{Math.round(item.temperatureMax)}</span>
                            <span className="forecast__low">{Math.round(item.temperatureMin)}</span>
                        </div>
                    )
            }

        })
    }
    render() {

        return (
                <Stagger
                    component="ul"
                    className={''}
                    key={new Date().getTime()}

                    transition="fadeIn" initialDelay={160} delay={90}>
                        {this._buildForecast()}
                </Stagger>
        )
    }
}

export default Forecast;
