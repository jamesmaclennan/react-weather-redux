import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ToggleSwitch from '@trendmicro/react-toggle-switch';

import '@trendmicro/react-toggle-switch/dist/react-toggle-switch.css';



import { removeLocation } from '../../actions/remove-location.js';

//

// This has turned into more of a container than a component - it should be moved

//

import Today from './location-today';
import Forecast from './location-forecast';
import POP from './location-pop';

class Location extends Component {
    constructor(props) {
        super(props);

        this.state = {
            hourly: false
        }
    }
    _buildTitle = () => {
        // make sure that actual city and state names are returned from google and
        // build title accordingly.
        let title = '';

        if (typeof this.props.location.name.city !== 'undefined') {
            title += this.props.location.name.city.long_name;
        }

        if (title.length >= 1) {
            title += ', ';
        }

        if (typeof this.props.location.name.state !== 'undefined') {
            title += this.props.location.name.state.short_name;
        }
        return (title);
    }
    _handleRemove = (e) => {
        e.preventDefault();
        this.props.removeLocation(this.props.id);
    }
    render() {
        return (
            <li className="location locations__item">
				<div className="location__wrapper">

                <header className="location__header">
                    <h2 className="location__title">{this._buildTitle()}</h2>
                    <a className="location__toggle" onClick={this._handleRemove}><i className="flaticon-delete"></i></a>
                </header>

                <Today location={this.props.location} />
                <div className="location__block location__block-secondary">
                    <div>
                        <h3 className="forecast-label">Forecast</h3>
                        <div className="toggle-hourly">
                            <span className="toggle-hourly__label">Hourly</span>
                            <ToggleSwitch className="toggle-hourly__toggle" checked={this.state.hourly} onChange={(e) => {
                                    this.setState({ hourly: !this.state.hourly });
                                }}
                            />
                        </div>
                    </div>
                    <Forecast location={this.props.location} hourly={this.state.hourly} key={this.props.id}/>
                </div>
                <POP location={this.props.location} hourly={this.state.hourly} />
				</div>
            </li>
        )
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({removeLocation: removeLocation}, dispatch);
}

export default connect(null,matchDispatchToProps)(Location);
