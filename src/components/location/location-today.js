// this is the top left section of a location's weather info
// it displays current conditions data

import React, { Component } from 'react';
import moment from 'moment';
import ReactAnimatedWeather from 'react-animated-weather';
import tz from 'moment-timezone';

class Today extends Component {
    _timezoneFormat(unixtime) {
        return moment.unix(unixtime).tz(this.props.location.weather.timezone).format('h:mm A')
    }
    render() {
        return (
        <div className="location__block location__block-primary">
            <div className="location__current-conditions">
                <div className="location__icon">
                    <ReactAnimatedWeather
                        icon={this.props.location.weather.currently.icon.split('-').join('_').toUpperCase()}
                        size={256}
                        color='#EF8354'
                    />
                </div>
                <div className="location__temperature">
                    <span>{Math.round(this.props.location.weather.currently.temperature)}</span>
                </div>
            </div>

            <ul className="today">
                <li>
                    <i className="today__icon flaticon-clock"></i>
                    <span className="today__value">{this._timezoneFormat(this.props.location.weather.currently.time)}</span>
                </li>
                <li>
                    <i className="today__icon flaticon-wind-sign"></i>
                    <span className="today__value">{Math.round(this.props.location.weather.currently.windSpeed)}KM/h</span>
                </li>
                <li>
                    <i className="today__icon flaticon-dawn"></i>
                    <span className="today__value">{this._timezoneFormat(this.props.location.weather.daily.data[0].sunriseTime)}</span>
                </li>
                <li>
                    <i className="today__icon flaticon-nature"></i>
                    <span className="today__value">{this._timezoneFormat(this.props.location.weather.daily.data[0].sunsetTime)}</span>
                </li>
            </ul>
        </div>
        )
    }
}

export default Today
