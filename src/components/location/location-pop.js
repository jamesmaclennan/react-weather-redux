// this is the POP chart displayed at the bottom of every location object

import React, { Component } from 'react';
import moment from 'moment';
import { Line } from 'react-chartjs-2';

class POP extends Component {
    _buildPOP(format='daily') {
        let forecastRange;
        this.props.hourly ? forecastRange = 'hourly' : forecastRange = 'daily';

        let POP = {
            labels:
                this.props.location.weather[forecastRange].data.slice(1,8).map((item, i) => {
                    switch(forecastRange) {
                        case 'hourly':
                            return (
                                moment.unix(item.time).tz(this.props.location.weather.timezone).format('hA')
                            )
                        default:
                            return (
                                moment.unix(item.time).format('ddd')
                            )
                    }
                })
            ,
            values:
                this.props.location.weather[forecastRange].data.slice(1,8).map((item, i) => {
                    return (
                        item.precipProbability*100
                    )
                })
        }
        return {
            labels: POP.labels,
            datasets: [
                {
                  label: 'Probability of Precipitation',
                  fill: true,
                  lineTension: 0.1,
                  backgroundColor: 'rgba(79, 93, 117, .25)',
                  borderColor: 'rgba(79, 93, 117, 1)',
                  borderCapStyle: 'round',
                  borderJoinStyle: 'bevel',
                  pointBorderColor: 'rgba(79, 93, 117, 1)',
                  pointBackgroundColor: '#fff',
                  pointHoverBackgroundColor: 'rgba(79, 93, 117, 1)',
                  pointHoverBorderColor: 'rgba(220,220,220,1)',
                  pointRadius: 1,
                  data: POP.values
                }
            ]
        }

    }
    render() {
        return (
            <div className="location__block location__block-tertiary">
                <Line
                    ref='chart'
                    data={this._buildPOP()}
                    height={200}
                    options={{
                        maintainAspectRatio: false,
                        labels: {
                            legend: {
                                position: 'top',
                            }
                        },

                        layout: {
                            padding: {
                                top: 10
                            }
                        },

                        scales: {
                            yAxes: [{
                                position: 'left',
                                scaleLabel: {
                                    display: true,
                                    labelString: "%",
                                    fontFamily: "Montserrat",
                                    fontColor: "black",
                                },
                                ticks: {
                                    min: 0,
                                    max: 100,
                                    stepSize: 25,
                                    reverse: false,
                                },
                            }]
                        }
                    }}
                />
            </div>
        )
    }
}

export default POP;
